# What the Corona Virus Pandemic Can Teach Us About Security Culture

A zine about the COVID-19 pandemic and how it can inform us about security culture.

## (Anti-)Copyright

This zine and repo and public domain under a CC0 license.
